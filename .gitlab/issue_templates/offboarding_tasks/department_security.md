## FOR SECURITY ONLY

- [ ] @jritchey @estrike: Remove the team member from HackerOne
- [ ] @jritchey @estrike: Remove the team member from hackerone-customer.slack.com slack workspace
- [ ] @jurbanc @mlancini: Remove the team member from Tenable.IO
- [ ] @jurbanc @mlancini: Remove the team member from Rackspace (Security Enclave)
- [ ] @jurbanc @mlancini: Remove the team member from AWS Security
- [ ] @jurbanc @blutz1: Remove the team member from Panther SIEM
- [ ] @rcshah @jburrows001 @mmaneval20: Remove the team member from ZenGRC
- [ ] @darawarde @sdaily: Remove the team member from Google Search Console
- [ ] @nsarosy: Remove team member from PhishLabs
